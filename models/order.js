/** 
*  Order model
*  Describes the characteristics of each attribute in an order resource.
*
* @author Preetham Potu <S533713@nwmissouri.edu>
*
*/

// bring in mongoose 
// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')
var Schema = mongoose.Schema;

const OrderSchema = new mongoose.Schema({

  _id: 
    {
    //  type: Schema.Types.ObjectId,
    //  ref:'customer_id',
    // required: true }
  
  // orderID: {
    type: Number,
    required: true,
    unique: true,
    // default: 555
  },
  
  datePlaced: {
    type: Date,
    required: true,
    default: Date.now()
  },
  dateShipped: {
    type: Date,
    required: true
  },
  paymentType: {
    type: String,
    required: true
  },
  paid: {
    type: Boolean,
    
  },
  customer_id: {
    required: true,
    type: Number

  }
})
module.exports = mongoose.model('Order', OrderSchema)
