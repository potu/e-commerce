/** 
*  Product model
*  Describes the characteristics of each attribute in an order resource.
*
* @author Preetham Potu <S533713@nwmissouri.edu>
*
*/
const mongoose = require('mongoose')

const ProductSchema = new mongoose.Schema({

  _id: { type: Number, required: true },
  productKey: {
    type: String,
    required: true,
    unique: true,
   
  },
  description: {
    type: String,
    required: false,
   
  },
  unitPrice: {
    type: Number,
    required: true,
    default: 9.99,
    min: 0,
    max: 100000
  }
})
module.exports = mongoose.model('Product', ProductSchema)
